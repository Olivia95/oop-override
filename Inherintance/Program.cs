﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inherintance
{
    class Program
    {
        static void Main(string[] args)
        {
            
            BaseClass baseChild = new ChildClass();
            int first = 3;
            int second = 6;
            var result = baseChild.AddCharacter(first,second);
            baseChild.ReturnResult(result);
            Console.WriteLine("The result for the sum of {0} and {1} passed to method AddCharacter is: {2}", first, second, result);

            Console.ReadKey();

        }
    }
}
